/**
 * isMobile.js v0.3.2
 *
 * A simple library to detect Apple phones and tablets,
 * Android phones and tablets, other mobile devices (like blackberry, mini-opera and windows phone),
 * and any kind of seven inch device, via user agent sniffing.
 *
 * @author: Kai Mallea (kmallea@gmail.com)
 *
 * @license: http://creativecommons.org/publicdomain/zero/1.0/
 */
(function (global) {

    var apple_phone      = /iPhone/i,
        apple_ipod       = /iPod/i,
        apple_tablet     = /iPad/i,
        android_phone    = /(?=.*\bAndroid\b)(?=.*\bMobile\b)/i, // Match 'Android' AND 'Mobile'
        android_tablet   = /Android/i,
        windows_phone    = /IEMobile/i,
        windows_tablet   = /(?=.*\bWindows\b)(?=.*\bARM\b)/i, // Match 'Windows' AND 'ARM'
        other_blackberry = /BlackBerry/i,
        other_opera      = /Opera Mini/i,
        other_firefox    = /(?=.*\bFirefox\b)(?=.*\bMobile\b)/i, // Match 'Firefox' AND 'Mobile'
        seven_inch = new RegExp(
            '(?:' +         // Non-capturing group

            'Nexus 7' +     // Nexus 7

            '|' +           // OR

            'BNTV250' +     // B&N Nook Tablet 7 inch

            '|' +           // OR

            'Kindle Fire' + // Kindle Fire

            '|' +           // OR

            'Silk' +        // Kindle Fire, Silk Accelerated

            '|' +           // OR

            'GT-P1000' +    // Galaxy Tab 7 inch

            ')',            // End non-capturing group

            'i');           // Case-insensitive matching

    var match = function(regex, userAgent) {
        return regex.test(userAgent);
    };

    var IsMobileClass = function(userAgent) {
        var ua = userAgent || navigator.userAgent;

        this.apple = {
            phone:  match(apple_phone, ua),
            ipod:   match(apple_ipod, ua),
            tablet: match(apple_tablet, ua),
            device: match(apple_phone, ua) || match(apple_ipod, ua) || match(apple_tablet, ua)
        };
        this.android = {
            phone:  match(android_phone, ua),
            tablet: !match(android_phone, ua) && match(android_tablet, ua),
            device: match(android_phone, ua) || match(android_tablet, ua)
        };
        this.windows = {
            phone:  match(windows_phone, ua),
            tablet: match(windows_tablet, ua),
            device: match(windows_phone, ua) || match(windows_tablet, ua)
        };
        this.other = {
            blackberry: match(other_blackberry, ua),
            opera:      match(other_opera, ua),
            firefox:    match(other_firefox, ua),
            device:     match(other_blackberry, ua) || match(other_opera, ua) || match(other_firefox, ua)
        };
        this.seven_inch = match(seven_inch, ua);
        this.any = this.apple.device || this.android.device || this.windows.device || this.other.device || this.seven_inch;
        // excludes 'other' devices and ipods, targeting touchscreen phones
        this.phone = this.apple.phone || this.android.phone || this.windows.phone;
        // excludes 7 inch devices, classifying as phone or tablet is left to the user
        this.tablet = this.apple.tablet || this.android.tablet || this.windows.tablet;
    };

    var IM = new IsMobileClass();
    IM.Class = IsMobileClass;

    if (typeof module != 'undefined' && module.exports) {
        module.exports = IM;
    } else if (typeof define === 'function' && define.amd) {
        define(IM);
    } else {
        global.isMobile = IM;
    }

})(this);

/*! jQuery ellipsis - v1.1.1 - 2014-02-23
* https://github.com/STAR-ZERO/jquery-ellipsis
* Copyright (c) 2014 Kenji Abe; Licensed MIT */
!function(a){a.fn.ellipsis=function(b){var c={row:1,onlyFullWords:!1,"char":"...",callback:function(){},position:"tail"};return b=a.extend(c,b),this.each(function(){var c=a(this),d=c.text(),e=d,f=e.length,g=c.height();c.text("a");var h=parseFloat(c.css("lineHeight"),10),i=c.height(),j=h>i?h-i:0,k=j*(b.row-1)+i*b.row;if(k>=g)return c.text(d),void b.callback.call(this);var l=1,m=0,n=d.length;if("tail"===b.position){for(;n>l;)m=Math.ceil((l+n)/2),c.text(d.slice(0,m)+b["char"]),c.height()<=k?l=m:n=m-1;d=d.slice(0,l),b.onlyFullWords&&(d=d.replace(/[\u00AD\w\uac00-\ud7af]+$/,"")),d+=b["char"]}else if("middle"===b.position){for(var o=0;n>l;)m=Math.ceil((l+n)/2),o=Math.max(f-m,0),c.text(e.slice(0,Math.floor((f-o)/2))+b["char"]+e.slice(Math.floor((f+o)/2),f)),c.height()<=k?l=m:n=m-1;o=Math.max(f-l,0);var p=e.slice(0,Math.floor((f-o)/2)),q=e.slice(Math.floor((f+o)/2),f);b.onlyFullWords&&(p=p.replace(/[\u00AD\w\uac00-\ud7af]+$/,"")),d=p+b["char"]+q}c.text(d),b.callback.call(this)}),this}}(jQuery);


Number.prototype.formatMoney = function(c, d, t){
var n = this, 
    c = isNaN(c = Math.abs(c)) ? 2 : c, 
    d = d == undefined ? "." : d, 
    t = t == undefined ? "," : t, 
    s = n < 0 ? "-" : "", 
    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
    j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 };

/**
 * codeJS v1.0.0
 *
 * A code validator library for bootstrap 3
 *
 * @author: Picha Mahakittikun (picha@syllistudio.com)
 *
 */
var codeJS = {
    
    duplicatedContainer : function() {
        $('.container, .container-fluid').each(function() {
            if($(this).parent().is('.container, .container-fluid')) {
                console.log('Duplicated .container or .container-fluid', $(this));
            } else if($(this).parent().closest('.container, .container-fluid').length > 0) {
                console.log('Duplicated .container or .container-fluid', $(this));
            }
        });
    },
    
    orphanRow : function() {
        $('.row').each(function() {
            if($(this).closest('.container, .container-fluid').length == 0) {
                console.log('Orphan .row', $(this));
            }
        });
    },
    
    orphanCol : function() {
        $('*[class^="col-"], *[class*=" col-"]').each(function() {
            if(!$(this).parent().is('.row')) {
                console.log('Orphan .col-*', $(this));
            }
        });
    },
    
    runAll : function() {
        this.duplicatedContainer();
        this.orphanRow();
        this.orphanCol();
    }
    
};


/**
 * bootstrapExt v1.0.0
 *
 * A utility library to help manipulating UI components
 *
 * @author: Picha Mahakittikun (picha@syllistudio.com)
 *
 */
var bootstrapExt = {
    
    init : function() {
        var o = this;
        $(window).resize(function() {
           o.resize(); 
        });
        this.sameHeight();
    },
    
    resize : function() {
        this.sameHeight();
        this.dynamicText();
    },
    
    sameHeight : function() {
        $('.col-xs-autoheight, .col-sm-autoheight, .col-md-autoheight, .col-lg-autoheight, .col-xs-sameheight, .col-sm-sameheight, .col-md-sameheight, .col-lg-sameheight').css('height', 'auto');
        var blocks = [];
        $('.col-xs-sameheight').each(function() {
            blocks.push($(this).parent().get(0));
        });
        blocks = $.unique(blocks);
        for(var i in blocks) {
            common.equalizeHeight($(blocks[i]).find('> .col-xs-sameheight'));
        }
        
        if($(window).width() >= 768) {
            var blocks = [];
            $('.col-sm-sameheight').each(function() {
                blocks.push($(this).parent().get(0));
            });
            blocks = $.unique(blocks);
            for(var i in blocks) {
                common.equalizeHeight($(blocks[i]).find('> .col-sm-sameheight'));
            }
            
            $('.col-sm-autoheight').css('height', 'auto');
        }
        
        if($(window).width() >= 992) {
            var blocks = [];
            $('.col-md-sameheight').each(function() {
                blocks.push($(this).parent().get(0));
            });
            blocks = $.unique(blocks);
            for(var i in blocks) {
                common.equalizeHeight($(blocks[i]).find('> .col-md-sameheight'));
            }
            
            $('.col-md-autoheight').css('height', 'auto');
        }
        
        if($(window).width() >= 1200) {
            var blocks = [];
            $('.col-lg-sameheight').each(function() {
                blocks.push($(this).parent().get(0));
            });
            blocks = $.unique(blocks);
            for(var i in blocks) {
                common.equalizeHeight($(blocks[i]).find('> .col-lg-sameheight'));
            }
            
            $('.col-lg-autoheight').css('height', 'auto');
        }
    },
    
    dynamicText : function() {
        // swap the content regarding the size of viewport if defined in attributes
        if(MOBILE()) { // phone
            $('*[data-mobile]').each(function() {
                var mobile = $(this).data('mobile');
                if(mobile) {
                    $(this).html(mobile);
                }
            });
        } else if(TABLET()) { // tablet portrait
            $('*[data-tablet]').each(function() {
                var tablet = $(this).data('tablet');
                if(tablet) {
                    $(this).html(tablet);
                }
            });
        } else {
            $('*[data-desktop]').each(function() {
                var desktop = $(this).data('desktop');
                if(desktop) {
                    $(this).html(desktop);
                }
            });
        }
    }
    
};

var common = {
    
    equalizeHeight : function(elements) {
        var maxH = 0;
        elements.css('height', 'auto');
        elements.each(function() {
            maxH = Math.max($(this).outerHeight(), maxH);
        });
        elements.css('height', maxH);
    },
    
    addParam : function(url, parameterName, parameterValue, atStart/*Add param before others*/){
        var replaceDuplicates = true;
        if(url.indexOf('#') > 0){
            var cl = url.indexOf('#');
            urlhash = url.substring(url.indexOf('#'),url.length);
        } else {
            urlhash = '';
            cl = url.length;
        }
        sourceUrl = url.substring(0,cl);

        var urlParts = sourceUrl.split("?");
        var newQueryString = "";

        if (urlParts.length > 1)
        {
            var parameters = urlParts[1].split("&");
            for (var i=0; (i < parameters.length); i++)
            {
                var parameterParts = parameters[i].split("=");
                if (!(replaceDuplicates && parameterParts[0] == parameterName))
                {
                    if (newQueryString == "")
                        newQueryString = "?";
                    else
                        newQueryString += "&";
                    newQueryString += parameterParts[0] + "=" + (parameterParts[1]?parameterParts[1]:'');
                }
            }
        }
        if (newQueryString == "")
            newQueryString = "?";

        if(atStart){
            newQueryString = '?'+ parameterName + "=" + parameterValue + (newQueryString.length>1?'&'+newQueryString.substring(1):'');
        } else {
            if (newQueryString !== "" && newQueryString != '?')
                newQueryString += "&";
            newQueryString += parameterName + "=" + (parameterValue?parameterValue:'');
        }
        return urlParts[0] + newQueryString + urlhash;
    },

    removeParam : function(url, parameter) {
      var urlparts= url.split('?');

      if (urlparts.length>=2)
      {
          var urlBase=urlparts.shift(); //get first part, and remove from array
          var queryString=urlparts.join("?"); //join it back up

          var prefix = encodeURIComponent(parameter)+'=';
          var pars = queryString.split(/[&;]/g);
          for (var i= pars.length; i-->0;)               //reverse iteration as may be destructive
              if (pars[i].lastIndexOf(prefix, 0)!==-1)   //idiom for string.startsWith
                  pars.splice(i, 1);
          url = urlBase+'?'+pars.join('&');
      }
      return url;
    },

    getParam : function(url, name) {
        return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(url)||[,""])[1].replace(/\+/g, '%20'))||null
    },
    
    isUrl : function(s) {
        var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
        return regexp.test(s);
    },

    isEmpty : function(val) {
        if (val === 0) {
            return false;
        }
        return val == null || val == '';
    },

    isEmail : function(text) {
        var regx = /^[a-zA-Z0-9\'`’._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        return !this.isEmpty(text) && regx.test(text);
    },
    
    isPhone : function(text) {
        var regx = /^\+?\d{10,15}$/;
        return !this.isEmpty(text) && regx.test(text);
    },
    
    isNoSymbol : function(text) {
        var regx = /([a-zA-Z' ]+)/;
        return this.isEmpty(text) || regx.test(text);
    },

    unique : function(list) {
        var result = [];
        $.each(list, function(i, e) {
        if ($.inArray(e, result) == -1) result.push(e);
        });
        return result;
    },

    nl2br : function(str, is_xhtml) {
        var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br/>' : '<br>';
        return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
    },
        
    encodeHTMLEntities : function(str) {
        return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
    },
    
    blockUI : function(selector, message) {
        (typeof selector === 'string' ? jQuery(selector) : selector).block({
            message: message || '',
            overlayCSS : {
                background: '#fff url(\'syjscss/ajax-loader.gif\') no-repeat center',
                opacity: 0.6
            }
        });
    },

    unblockUI : function(selector) {
        (typeof selector === 'string' ? jQuery(selector) : selector).unblock();
    },

    setCookie : function(c_name, value, exdays) {
        var exdate = new Date();
        exdate.setDate(exdate.getDate() + exdays);
        var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
        document.cookie=c_name + "=" + c_value;
    },

    getCookie : function(c_name) {
        var c_value = document.cookie;
        var c_start = c_value.indexOf(" " + c_name + "=");
        if (c_start == -1)
          {
          c_start = c_value.indexOf(c_name + "=");
          }
        if (c_start == -1)
          {
          c_value = null;
          }
        else
          {
          c_start = c_value.indexOf("=", c_start) + 1;
          var c_end = c_value.indexOf(";", c_start);
          if (c_end == -1)
          {
        c_end = c_value.length;
        }
        c_value = unescape(c_value.substring(c_start,c_end));
        }
        return c_value;
    },

    smoothScroll : function(elem, offset, delay) {
        var y = 0;
        if(elem != null) {
            y = elem.offset().top;
        }
        if(offset) {
            y = y + offset;
        }
        if(!delay) {
            delay = 1000;
        }
        $('html, body').animate({ scrollTop : y }, delay);
    },
    
    enableSubmitByEnter : function(fields) {
        fields.keypress(function(e) {
            if (e.which == 13) {
                e.preventDefault();
                $(this).closest('form').find('.reqsubmit').trigger('click');
            }
        });
    },

    setInvalid : function(formControl) {
        formControl.addClass('invalid');
        formControl.closest('.form-group').addClass('has-error');
    },
    
    resetValidate : function(elements) {
        var o = this;
        elements.removeClass('invalid');
        elements.each(function() {
            $(this).closest('.form-group').removeClass('has-error has-warning has-success');
        });
    },
    
    autoValidate : function(elements) {
        var o = this;
        var valid = true;
        o.resetValidate(elements);
        elements.each(function() {
            var val = $(this).val();

            if($(this).is('.nosym')) {
                if($(this).is('.req')) {
                    if(o.isEmpty(val) || !o.isNoSymbol(val)) {
                        o.setInvalid($(this));
                        valid = false;
                    }
                } else {
                    if(!o.isEmpty(val) && !o.isNoSymbol(val)) {
                        o.setInvalid($(this));
                        valid = false;
                    }
                }
            } else if($(this).is('.tel')) {
                if($(this).is('.req')) {
                    if(!o.isPhone(val)) {
                        o.setInvalid($(this));
                        valid = false;
                    }
                } else {
                    if(!o.isEmpty(val) && !o.isPhone(val)) {
                        o.setInvalid($(this));
                        valid = false;
                    }
                }
            } else if($(this).is('.em')) {
                if($(this).is('.req')) {
                    if(!o.isEmail(val)) {
                        o.setInvalid($(this));
                        valid = false;
                    }
                } else {
                    if(!o.isEmpty(val) && !o.isEmail(val)) {
                        o.setInvalid($(this));
                        valid = false;
                    }
                }
            } else if($(this).is('.url')) {
                if($(this).is('.req')) {
                    if(!o.isUrl(val)) {
                        o.setInvalid($(this));
                        valid = false;
                    }
                } else {
                    if(!o.isEmpty(val) && !o.isUrl(val)) {
                        o.setInvalid($(this));
                        valid = false;
                    }
                }
            } else if($(this).is('.req')) {
                if(o.isEmpty(val)) {
                    o.setInvalid($(this));
                    valid = false;
                }
            }
        });

        return valid;
    },
    
    CONTROL_KEYS : [ 16, 8, 9, 37, 38, 39, 40, 17, 18, 91 ], // shift, backspace, tab, arrows, ctrl, alt, command
    
    init : function() {
        var o = this;
        // make tel field can only input '+' and '0-9'
        $('input.tel, textarea.tel').keydown(function(e) {
            var code = event.keyCode ? event.keyCode : event.which;
            if(o.CONTROL_KEYS.indexOf(code) != -1) {
                return;
            }
            var whitelist = [ 187, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 107, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105 ];
            if(whitelist.indexOf(code) == -1) {
                e.preventDefault();
            }
        });
        o.enableSubmitByEnter($('input.entersubmit'));
        $('input.nosym, textarea.nosym').keydown(function(e) {
            var code = event.keyCode ? event.keyCode : event.which;
            if(o.CONTROL_KEYS.indexOf(code) != -1) {
                return;
            }
            if(!common.isNoSymbol(String.fromCharCode(code))) {
                e.preventDefault();
            }
        });
    }

};

var ellipsis = {
    
    wrap : function(elem) {
        var row = elem.data('row');
        var fullword = elem.data('fullword');
        var original = elem.data('originaltext');
        var customChar = elem.data('char');
        if(common.isEmpty(original)) {
            elem.data('originaltext', elem.text());
        } else {
            elem.html(original);
        }
        if (common.isEmpty(customChar)) {
            customChar = '...';
        }
        elem.ellipsis({
            row : row || 1,
            onlyFullWords : fullword || false,
            'char' : customChar
        });
    },
    
    wrapAll : function() {
        var o = this;
		$('.wrap').each(function() {
			o.wrap($(this));
		});
    },
    
    resize : function() {
        this.wrapAll();
    },
    
    init : function() {
        var o = this;
        
        setTimeout(function(){
            o.wrapAll();
        }, 800);
        
        $(window).resize(function() {
            o.resize();
        });
    }
    
};

function MOBILE() {
    return $(window).width() < 768;
}
function TABLET() {
    return !MOBILE() && $(window).width() < 992;
}
function DESKTOP() {
    return !MOBILE() && !TABLET();
}

function isMD() {
    return $(window).width() >= 992 && !isLG();
}
function isLG() {
    return $(window).width() >= 1200;
}

var $ = jQuery;
$(document).ready(function() {
    bootstrapExt.init();
    common.init();
    ellipsis.init();
    
    //codeJS.runAll();
});